(function ($) {
  Drupal.behaviors.viewReferenceTabs = {
    attach: function (context) {
      $('.view-reference-tabs', context).once('view-reference-tabs-processed').tabs();
    }
  };
})(jQuery);
